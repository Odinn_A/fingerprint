import React, { useState, useEffect, forwardRef } from 'react';
import DatePicker from 'react-datepicker';
import { useDispatch } from 'react-redux';
import '../../../node_modules/react-datepicker/dist/react-datepicker.css'
import { setMonthAndYear } from '../../modules/redux/reducersAndActions/monthsCalendar/actions';
import './index.css'

const MonthsCalendar = () => {
    const [startDate, setStartDate] = useState(new Date());
    const currentMonth = startDate.getMonth() + 1;
    const currentYear = startDate.getFullYear();
    const [month, setMonth] = useState(currentMonth);
    const [year, setYear] = useState(currentYear);
    const dispatch = useDispatch();
    dispatch(setMonthAndYear({month, year}));

    const CustomInput = forwardRef(({onClick}, ref) => (
        <button className='example-custom-input' onClick={onClick} ref={ref}></button>
    ));

    useEffect(() => {
        setMonth(startDate.getMonth() + 1);
        setYear(startDate.getFullYear());
    }, [startDate]); 

    return (
        <div className='month_calendar'>
            <div className='calendar_img'>
            <DatePicker
                selected={startDate}
                onChange={(date) => setStartDate(date)}
                maxDate={new Date()}
                showMonthYearPicker
                showMonthYearPicker
                customInput={<CustomInput />}
            />
            </div>
        </div>
    );
}

export default MonthsCalendar