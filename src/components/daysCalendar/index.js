import React, { useState, useEffect, forwardRef } from 'react';
import DatePicker from 'react-datepicker';
import { useDispatch } from 'react-redux';
import '../../../node_modules/react-datepicker/dist/react-datepicker.css'
import { setDATE } from '../../modules/redux/reducersAndActions/daysCalendar/actions';
import './index.css'

const DaysCalendar = () => {
    const [startDate, setStartDate] = useState(new Date());
    const currentDay = startDate.getDate();
    const currentMonth = startDate.getMonth() + 1;
    const currentYear = startDate.getFullYear();

    const [day, setDay] = useState(currentDay);
    const [month, setMonth] = useState(currentMonth);
    const [year, setYear] = useState(currentYear);
    const dispatch = useDispatch();
    dispatch(setDATE({day, month, year}));

    const CustomInput = forwardRef(({ value, onClick }, ref) => (
        <button className='customInput' onClick={onClick} ref={ref}>{value}</button>
    ));

    useEffect(() => {
        setDay(startDate.getDate());
        setMonth(startDate.getMonth() + 1);
        setYear(startDate.getFullYear())
    }, [startDate]); 

    return (
        <div className='days_calendar'>
            <DatePicker
                selected={startDate}
                onChange={(date) => setStartDate(date)}
                minDate={new Date(new Date().setDate(1))}
                maxDate={new Date()}
                dateFormat='dd.MM.yyyy'
                className='customInput'               
                customInput={<CustomInput />}
            />
        </div>
    );
}

export default DaysCalendar