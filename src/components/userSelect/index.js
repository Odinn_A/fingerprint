import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedUserId } from '../../modules/redux/reducersAndActions/userSelect/actions';
import { usersSelector } from '../../modules/redux/reducersAndActions/userSelect/selectors';
import './index.css'
import { Scrollbars } from 'react-custom-scrollbars';

const ListOfUsers = () => {
    const users = useSelector(usersSelector);
    const dispatch = useDispatch();

    const initialData = useSelector(state => state.authReducer.initialData);

    useEffect(() => {
        const selected = document.querySelector(".selected");
        const optionsContainer = document.querySelector(".options-container");
        const optionsList = document.querySelectorAll(".user");

        selected.addEventListener("click", () => {
            optionsContainer.classList.toggle("active");
        });

        optionsList.forEach(option => {
            option.addEventListener("click", () => {
                selected.innerHTML = option.querySelector("label").innerHTML;
                optionsContainer.classList.remove("active");
            });
        });
    });

    function disableDropdown() {
        document.getElementsByClassName('userDropdown').style.display = 'none';
    }

    return (
        <div className='listOfUsers'>
            <div className="select-box dropdownContainer" tabindex="-1">                       
                <div className="selected threeDots">{initialData[0]?.userProfile?.firstname_lastname}</div>
                <div className="userDropdown">
                    <p className="users_text">Users
                        <button className="closeBtn" onClick={disableDropdown}>
                            <div class="close-container">
                                <div class="leftright"></div>
                                <div class="rightleft"></div>
                                <label class="close"></label>
                            </div>
                        </button>
                    </p>
                    <hr className="users_line"/>
                    <div class="options-container">
                        <Scrollbars autoHeight autoHeightMax={500}>
                            {users?.map(user => 
                                <div className="user" onClick={() => dispatch(setSelectedUserId(user.user_id))}>
                                    <label className="userOption">{user.firstname_lastname}</label>
                                </div>  
                            )}
                        </Scrollbars>                   
                    </div>                                           
                </div>
            </div>
        </div>
    );
}

export default ListOfUsers