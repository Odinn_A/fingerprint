import "./styles.css";
import teamLogo from "../../assets/svg/teamLogo.svg";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import { selectInitialData } from "../../modules/redux/reducersAndActions/header/selectors";
import { localization } from "../../modules/localization";
import { ROUTES } from "../../services/routes";
import { reset } from "../../modules/redux/reducersAndActions/authorization/action"
import { setGenTimesheet } from "../../modules/saga/generateTimesheet/action"


function Header() {

    const initialData = useSelector(selectInitialData());

    const roles = initialData[0]?.roles;
    const [position, setPosition] = useState("");
    const [isAdmin, setIsAdmin] = useState("");
    const userProfile = initialData[0]?.userProfile;

    const dispatch = useDispatch();

    useEffect(() => {
        if (userProfile?.role_id == "1") {
            setPosition(roles[0]?.role_text);
            setIsAdmin(true)
        }
        else if (userProfile?.role_id == "2") {
            setPosition(roles[1]?.role_text);
            setIsAdmin(true)
        }
        else {
            setPosition(roles[2]?.role_text);
            setIsAdmin(false)
        }
    }, []);

    function disableDropdown() {
        document.getElementsByClassName('dropdown').style.display = 'none';
    }

    return (
        <div className="headerContainer">
            <div className="logoAndUserInfo">
                <img alt="logoImage" className="logoImage" src={teamLogo} />
                <div className="userInfo">{`${userProfile?.firstname_lastname} / ${position}`}</div>
            </div>
            <div className="menuNav">
                <div className="dropdownContainer" tabindex="-1">
                    <div id="menuItem1" className="threeDots">{localization.TIME}</div>
                    <div className="dropdown1" >
                        <p className='header_button_list'>{localization.TIME_EDIT}
                            <button className="closeBtn" onClick={disableDropdown}>
                                <div class="close-container">
                                    <div class="leftright"></div>
                                    <div class="rightleft"></div>
                                    <label class="close"></label>
                                </div>
                            </button>
                        </p>
                        <hr className="header_button_list_line"/>
                        <NavLink to="" className="navLink"><div>{localization.EDIT_TIME}</div></NavLink>
                        <NavLink to="" className="navLink" style={{ display: isAdmin ? 'block' : 'none' }}><div >{localization.EDIT_TIME_ALL}</div></NavLink>
                    </div>
                </div>
                <div id="menuItem2" className="threeDots" onClick={() => dispatch(setGenTimesheet())}>{localization.TIMESHEET}</div>
                <div className="dropdownContainer" tabindex="-1">
                    <div id="menuItem3" className="threeDots">{localization.PROFILE}</div>
                    <div className="dropdown" >
                        <p className='header_button_list'>{localization.PROFILE}
                            <button className="closeBtn" onClick={disableDropdown}>
                                <div class="close-container">
                                    <div class="leftright"></div>
                                    <div class="rightleft"></div>
                                    <label class="close"></label>
                                </div>
                            </button>
                        </p>
                        <hr className="header_button_list_line"/>
                        <NavLink to="" className="navLink"><div>{localization.EDIT_PROFILE}</div></NavLink>
                        <NavLink to="" className="navLink"><div>{localization.CHANGE_PASSWORD}</div></NavLink>
                        <NavLink to={ROUTES.SIGN_IN} className="navLink" ><div className="logOut" onClick={() => dispatch(reset())}>{localization.LOG_OUT}</div></NavLink>
                    </div>
                </div>
            </div>
        </div>

    )
}
export default Header;

