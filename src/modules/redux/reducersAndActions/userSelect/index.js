import { userSelectTypes } from "./actionTypes";

const initialState = {
    users: [],
    selectedUserId: null,
};

function userSelectReducer (state = initialState, action) {
    switch (action.type) {
        case userSelectTypes.SET_USERS_LIST:
            return {
                ...state,
                users: action.payload,
            }
        case userSelectTypes.SET_SELECTED_USER_ID:
            return {
                ...state,
                selectedUserId: action.payload,
            }
        default:
            return state
    }
}

export default userSelectReducer;