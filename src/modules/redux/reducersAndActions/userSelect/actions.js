import { userSelectTypes } from "./actionTypes"

export const setUsersList = (payload) => ({
    type: userSelectTypes.SET_USERS_LIST,
    payload,
});

export const setSelectedUserId = (payload) => ({
    type: userSelectTypes.SET_SELECTED_USER_ID,
    payload,
});