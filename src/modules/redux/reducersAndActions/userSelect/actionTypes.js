export const userSelectTypes = {
    SET_USERS_LIST: 'SET_USERS_LIST',
    SET_SELECTED_USER_ID: 'SET_SELECTED_USER_ID',
}