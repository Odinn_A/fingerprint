import { types } from "./actionTypes"

const initialState = {
  token: "",
  initialData: [],
  error: "",
  isError: "",
  isChangePass: false,
  isUserAuthorised: false
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.NEW_SIGN_IN_TOKEN:
      return {
        ...state,
        token: action.payload,
      };
    case types.NEW_SIGN_IN_DATA:
      return {
        ...state,
        initialData: [...action.payload],
      };
    case types.RESET:
      return {
        ...initialState
      }
      case types.GET_ERROR: 
      return {
      ...state,
      error: action.payload,
      isError: action.payload
  }
  case types.IS_CHANGE_PASS:
     return {
      ...state,
      isChangePass: action.payload
  }
  case types.IS_USER_AUTHORISED:
     return {
      ...state,
      isUserAuthorised: action.payload
  }
    default:
      return state;
  }
};

export default authReducer;
