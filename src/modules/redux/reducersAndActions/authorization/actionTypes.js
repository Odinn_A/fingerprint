export const types = ({
    GET_ERROR: "GET_ERROR",
    GET_PASSWORD : "GET_PASSWORD",
    NEW_SIGN_IN_TOKEN: "NEW_SIGN_IN_TOKEN",
    NEW_SIGN_IN_DATA: "NEW_SIGN_IN_DATA",
    RESET: "RESET",
    IS_CHANGE_PASS: "IS_CHANGE_PASS",
    IS_USER_AUTHORISED: "IS_USER_AUTHORISED"
});
