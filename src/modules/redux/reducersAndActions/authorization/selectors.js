export const selectIsUserAuthorised = (state) => state.authReducer.isUserAuthorised;

export const selectIsChangePassword = (state) => state.authReducer.isChangePass;