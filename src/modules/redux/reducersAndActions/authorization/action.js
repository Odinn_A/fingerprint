import { types } from "./actionTypes";

export const getError = (payload) => ({
    type: types.GET_ERROR,
    payload
});

export const signInToken = (payload) => ({
    type: types.NEW_SIGN_IN_TOKEN,
    payload
});

export const signInData = (payload) => ({
    type: types.NEW_SIGN_IN_DATA,
    payload
})

export const reset = () => ({
    type: types.RESET
});

export const setIsChangePassword = (payload) => ({
    type: types.IS_CHANGE_PASS,
    payload
});

export const setIsUserAuthorised = (payload) => ({
    type: types.IS_USER_AUTHORISED,
    payload
})