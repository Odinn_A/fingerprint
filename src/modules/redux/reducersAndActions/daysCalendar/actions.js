import { dayCalendarTypes } from "./actionTypes";

export const setDATE = (payload) => ({
    type: dayCalendarTypes.SET_DATE,
    payload
})