export const selectDay = (state) => state.dayCalendarReducer.day;
export const selectMonth = (state) => state.dayCalendarReducer.month;
export const selectYear = (state) => state.dayCalendarReducer.year;