import { dayCalendarTypes } from "./actionTypes";

const initialState = {
    day: null,
    month: null,
    year: null,
};

function dayCalendarReducer (state = initialState, action) {
    switch (action.type) {
        case dayCalendarTypes.SET_DATE:
            return {
                ...state,
                day: action.payload.day,
                month: action.payload.month,
                year: action.payload.year,
            }
        default:
            return state 
    }
}

export default dayCalendarReducer;