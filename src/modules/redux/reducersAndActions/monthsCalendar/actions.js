import { monthCalendarTypes } from "./actionTypes";

export const setMonthAndYear = (payload) => ({
    type: monthCalendarTypes.SET_MONTH_AND_YEAR,
    // month: payload.month,
    // year: payload.year,
    payload
})