import { monthCalendarTypes } from "./actionTypes";

const initialState = {
    month: null,
    year: null,
};

function monthCalendarReducer (state = initialState, action) {
    switch (action.type) {
        case monthCalendarTypes.SET_MONTH_AND_YEAR:
            return {
                ...state,
                month: action.payload.month,
                year: action.payload.year,
            }
        default:
            return state 
    }
}

export default monthCalendarReducer;