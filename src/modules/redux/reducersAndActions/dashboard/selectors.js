export const timesheetSelector = (state) => state?.dashboardReducer.timesheet;
export const totalTimeSelector = (state) => state?.dashboardReducer.total;