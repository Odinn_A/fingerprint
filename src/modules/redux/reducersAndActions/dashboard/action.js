import { actionTypes } from "./actionTypes"

export const setTimesheet = (payload) => ({
    type: actionTypes.SET_TIMESHEET,
    payload,
});

export const setTotal = (payload) => {
    return({
    type:actionTypes.SET_TOTAL,
    payload
})};