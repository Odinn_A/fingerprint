import { actionTypes } from "./actionTypes";

const initialState = {
    timesheet: [],
    total: []
};
function dashboardReducer(state = initialState, action) {
    console.log(`state.timesheet`, state.timesheet)
    switch (action.type) {
        case actionTypes.SET_TIMESHEET:
            return {
                ...state,
                timesheet: [...action.payload]
            }
            case actionTypes.SET_TOTAL:
            return{
                ...state,
                total: [...state.total, ...action.payload],
            }
        default:
            return state;
    }
}
        
export default dashboardReducer;