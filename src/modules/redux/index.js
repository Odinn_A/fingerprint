import createSagaMiddleware from "@redux-saga/core";
import { applyMiddleware, combineReducers, createStore } from "redux";
import { rootSaga } from "../saga";
import dashboardReducer from "./reducersAndActions/dashboard";
import authReducer from './reducersAndActions/authorization/index'
import { persistStore, persistReducer } from 'redux-persist';
import localStorage from 'redux-persist/lib/storage'
import userSelectReducer from './reducersAndActions/userSelect/index';
import monthCalendarReducer from "./reducersAndActions/monthsCalendar";
import dayCalendarReducer from './reducersAndActions/daysCalendar/index'

const authPersistConfig = {
    key: 'root',
    storage: localStorage,
    whitelist: ['token', 'isChangePass', 'isUserAuthorised', 'initialData']
};

const reducers = combineReducers({
    dashboardReducer,
    authReducer: persistReducer(authPersistConfig, authReducer),
    userSelectReducer,
    monthCalendarReducer,
    dayCalendarReducer,
});

const middleware = createSagaMiddleware();

export const store = createStore(reducers, applyMiddleware(middleware))

middleware.run(rootSaga);

export const persistedStore = persistStore(store)
