import { setUsersList } from "../../redux/reducersAndActions/userSelect/actions";
import { takeEvery , call, put, select } from "redux-saga/effects";
import { userSelectTypes } from "./actionTypes";
import { apiRoutes } from "../../../services/apiRoutes";

let myHeaders = new Headers();

let raw = JSON.stringify({
    'cmd': 'GETLOGIN',
});

const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
};

const getUsersList = async (url) => {
    const response = await fetch(url, requestOptions)
      .then((res) => res.json())
      .catch((e) => console.warn("ERROR", e));
    return response.message;
  };
  
  function* getUsersListWorker() {
      const token = yield select(state => state.authReducer.token);
      myHeaders.append('Content-Type','application/json');
      myHeaders.append('Authorization', `Bearer ${token}`)
      const result = yield call(getUsersList, apiRoutes.baseUrl + apiRoutes.usersList);
      if (result) {
        yield put(setUsersList(result));
      }
  }
  
  export function* getUsersListWatcher() {
    yield takeEvery(userSelectTypes.GET_USERS_LIST , getUsersListWorker);
  }