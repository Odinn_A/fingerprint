import { userSelectTypes } from "./actionTypes"

export const getUsersList = (payload) => ({
    type: userSelectTypes.GET_USERS_LIST,
    payload
});