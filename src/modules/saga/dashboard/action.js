import { dashboardType } from "./actionTypes"

export const getTimesheet = (payload) => ({
    type: dashboardType.GET_TIMESHEET,
    payload
});