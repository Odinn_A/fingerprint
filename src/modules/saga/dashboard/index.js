import { setTimesheet } from "../../redux/reducersAndActions/dashboard/action";
import { takeEvery, call, put, select } from "redux-saga/effects";
import { dashboardType } from "./actionTypes";
import { apiRoutes } from "../../../services/apiRoutes";


var myHeaders = new Headers();

var raw = JSON.stringify({
    "cmd": "VIEWTIME",
    "user_id":19,
    "year": 2021,
    "month": 10
});

var requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: raw,
    redirect: 'follow'
};

export function* getTimesheetWatcher() {
    yield takeEvery(dashboardType.GET_TIMESHEET, getTimesheetWorker);
};

function* getTimesheetWorker() {
    const token = yield select(state => state.authReducer.token)
    myHeaders.append("Content-Type", "application/json");
    myHeaders.append("Authorization", `Bearer ${token}`);
    const resultResponse = yield call(getTimesheet, apiRoutes.baseUrl + apiRoutes.timesheetUrl);
    if (resultResponse?.error) {
        // Handle error from BAD AUTHORIZATION and others
    } else {
        const results = yield call(normilizeData, resultResponse);
        yield put(setTimesheet(results))
    }
};

const getTimesheet = async (url) => {
    const response = await fetch(url, requestOptions)
        .then((res) => res.json())
        .catch((e) => e);
    return response;
};

const normilizeData = (data) => {
    const keysFromObj = Object.keys(data.message);
    const valuesFromObj = Object.values(data.message)
    const newObjData = valuesFromObj.map((item,index) => {
        return {
            date: keysFromObj[index],
            time: item,
        }
    })
    return newObjData;
};