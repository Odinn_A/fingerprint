import { takeEvery, call, select } from 'redux-saga/effects'
import { apiRoutes } from '../../../services/apiRoutes';
import { types } from './actionTypes';

const date = new Date();

const createBodyGenTimesheet = (id) => JSON.stringify({
  "cmd": "TIMESHEET",
  "user_id": id,
  "year": date.getFullYear(),
  "month": date.getMonth() + 1
})

const genTimesheet = async (url, method, headers, body) => {
  const res = await fetch(url, {
    method,
    headers,
    body,
  })
  .then(response => response.json())
  .catch(error => console.log('error', error));
  return res;
}
const myHeaders = new Headers();

function* workerGenTimesheet() {
  const token = yield select(state => state.authReducer.token)
  const headers = {
    "Authorization": `Bearer ${token}`,
    "Content-Type": "application/json"
  }
  const id = yield select(state => state.authReducer.initialData[0].userProfile.user_id)
  const body = createBodyGenTimesheet(id)
  const result = yield call(genTimesheet, apiRoutes.baseUrl + apiRoutes.gentimesheet, 'POST', headers, body)
  alert(result.message)
}

export function* watcherGenTimesheet() {
  yield takeEvery(types.SEND_TIMESHEET, workerGenTimesheet)
}