import { types } from "./actionTypes";


export const setGenTimesheet = (payload) => ({
    type: types.SEND_TIMESHEET,
    payload
})