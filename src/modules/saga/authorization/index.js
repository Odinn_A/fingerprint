import { takeEvery, call, put, select } from "redux-saga/effects"
import { getError, setIsChangePassword, setIsUserAuthorised } from "../../redux/reducersAndActions/authorization/action";
import { types } from "./actionTypes";
import { apiRoutes } from "../../../services/apiRoutes";
import { signInData, signInToken } from "../../redux/reducersAndActions/authorization/action";

const createBodyForChangePass = (oldPassword, newPassword) => JSON.stringify({
  "cmd": "CHANGEPASSWORD",
  "data": {
    "old_password": oldPassword,
    "new_password": newPassword
  }
});

const changePassApi = async (url, requestOptions) => {
  const res = await fetch(url, requestOptions)
    .then(response => response.json())
    .catch(error => error);
  return res
}

export function* watcherChangePass() {
  yield takeEvery(types.CHANGE_PASS_SAGA, workerChangePass)
}

function* workerChangePass(action) {
  const token = yield select(state => state.authReducer.token)
  const myHeaders = new Headers();
  myHeaders.append("Authorization", `Bearer ${token}`);
  myHeaders.append("Content-Type", "application/json");
  if (action.payload.newPassword === action.payload.confirmPassword) {
    const reg = new RegExp(/(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{8,16}/g);
    const newPassTest = reg.test(action.payload.newPassword);
    if (newPassTest) {
      const body = createBodyForChangePass(action.payload.oldPassword, action.payload.newPassword);
      const response = yield call(changePassApi, `${apiRoutes.baseUrl}${apiRoutes.changePassword}`, {
        method: 'POST',
        headers: myHeaders,
        body,
      })
      alert(response.message)
    }
    else {
      if (6 > action.payload.newPassword.length || action.payload.newPassword.length > 12) {
        yield put(getError("Incorrect length! Length must be between 8 and 16 symbols"));
      }
      else if (!action.payload.newPassword.match("[!@#$%^&*]")) {
        yield put(getError("Password must contain any symbol"));
      }
      else if (!action.payload.newPassword.match("[0-9]")) {
        yield put(getError("Password must contain any number"));
      }
      else if (!action.payload.newPassword.match("[A-Z]")) {
        yield put(getError("Password must contain any uppercase letter"));
      }
      else if (!action.payload.newPassword.match("[a-z]")) {
        yield put(getError("Password must contain any lowercase letter"));
      }
    }
  }
  else {
    yield put(getError("Passwords didn't match"));
  }
}

const createBodyForSignIn = (login, password) =>
  JSON.stringify({
    name: login,
    password: password,
  });



const signInSaga = async (url, requestOptions) => {
  const response = await fetch(url, requestOptions)
    .then((res) => res.json())
    .catch((e) => console.warn("sigInSaga", e));
  return response;
};

function* signInWorker(action) {
  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");
  const body = createBodyForSignIn(action.payload.login, action.payload.password);
  const result = yield call(
    signInSaga,
    `${apiRoutes.baseUrl}${apiRoutes.signIn}`,
    {
      method: "POST",
      headers: myHeaders,
      body,
    }
  );

  if (result) {
    yield put(signInToken(result.token_data[0].token));
    yield put(signInData(result.initial_data));
    // yield put(setIsChangePassword(false));
    yield put(setIsChangePassword(result.initial_data[0].userProfile.change_password));
    yield put(setIsUserAuthorised(true));
  } else {
    console.error("Server Error")
  }
}

export function* signInWatcher() {
  yield takeEvery(types.SIGN_IN, signInWorker);
}
