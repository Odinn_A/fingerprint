import { types } from "./actionTypes"

export const setChangePassword = (payload) =>({
    type: types.CHANGE_PASS_SAGA,
    payload,
});

export const signIn = (payload) => ({
    type: types.SIGN_IN,
    payload
});