import { spawn } from "@redux-saga/core/effects";
import { watcherChangePass } from "./authorization";
import { signInWatcher } from "./authorization";
import { getTimesheetWatcher } from "./dashboard";
import { watcherGenTimesheet } from "./generateTimesheet";
import { getUsersListWatcher } from "./userSelect";

export function* rootSaga() {
    yield spawn(signInWatcher);
    yield spawn(getTimesheetWatcher);
    yield spawn(watcherGenTimesheet)
    yield spawn(watcherChangePass);
    yield spawn(getUsersListWatcher);
}