export const localization = {
    SIGN_IN_BTN: 'SIGN IN',
    LOGIN: 'LOGIN',
    PASSWORD: 'PASSWORD',
    TIME: 'TIME',
    TIME_EDIT: "TimeEdit",
    EDIT_TIME: "Edit Time",
    EDIT_TIME_ALL: "Edit Time All",
    TIMESHEET: "TIMESHEET",
    PROFILE: "PROFILE",
    EDIT_PROFILE: "Edit Profile",
    CHANGE_PASSWORD: "Change Password",
    LOG_OUT: "Log Out"
}