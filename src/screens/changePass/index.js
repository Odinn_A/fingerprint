import "./index.css";
import teamLogo from "../../assets/svg/teamLogo.svg";
import abstract from "../../assets/svg/authorization/abstract.svg";
import bg_right from "../../assets/svg/authorization/bg_right.png";
import plant from "../../assets/svg/authorization/plant.png";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setChangePassword } from "../../modules/saga/authorization/action";
import { getError } from "../../modules/redux/reducersAndActions/authorization/action";
import ClosedEye from "../../assets/svg/authorization/closed_eye";
import OpenEye from "../../assets/svg/authorization/open_eye";
import bg_left from "../../assets/svg/authorization/bg_left.png";

const ChangePasswordPage = () => {
  const error = useSelector(state => state?.authReducer.error);
  const isError = useSelector(state => state?.authReducer.isError);
  const dispatch = useDispatch();
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("", false);
  const [isShow, setIsShow] = useState(false);

  const onPressEnter = e => {
    e.key === "Enter" &&
      dispatch(
        setChangePassword({
          oldPassword,
          newPassword,
          confirmPassword,
        })
      );
  };
  useEffect(() => {
    error && dispatch(getError(""));
  }, [oldPassword, newPassword, confirmPassword]);

  return (
    <div className="changePasswordPageMain">
        <img src={bg_left} alt="bg_left" className='bgLeft'></img>
      <div className="wrapperLogo">
        <img src={teamLogo} alt="logo" className="logo" />
      </div>
      <div className="abstractAndInputs">
        <img src={abstract} alt="abstract" className='abstract' />
        <div className="inputsWrapper">
          <div className="inputs">
            <button className="openEye" onClick={() => setIsShow(!isShow)}>
              {isShow ? (
                <div>
                  <ClosedEye isOpacity={oldPassword} color={"#455A64"} />
                </div>
              ) : (
                <OpenEye isOpacity={oldPassword} color={"#455A64"} />
              )}
            </button>
            <input
              type={!isShow ? "password" : "text"}
              placeholder="Your password"
              onKeyDown={onPressEnter}
              onChange={e => setOldPassword(e.target.value)}
              className="changeInput"
            ></input>
          </div>
          <div className="inputsWrapper">
            <input
              type={!isShow ? "password" : "text"}
              placeholder="New Password"
              onKeyDown={onPressEnter}
              onChange={e => setNewPassword(e.target.value)}
              className="changeInput"
              id={isError ? "changeInputErr" : "changeInput"}
            ></input>
          </div>
          <div className="inputsWrapper">
            <input
              type={!isShow ? "password" : "text"}
              placeholder="Confirm password"
              onKeyDown={onPressEnter}
              onChange={e => setConfirmPassword(e.target.value)}
              className="changeInput"
              id={isError ? "changeInputErr" : "changeInput"}
            ></input>
          </div>

          <button
            className="changeButton"
            onClick={() => dispatch(setChangePassword({ oldPassword, newPassword, confirmPassword }))}
          >
            Change
          </button>
          {error ? <p id="error">{error}</p> : null}
          <img src={bg_right} alt="bg_right" className="bgRight" />
          <img src={plant} alt="plant" className="plant" />
        </div>
      </div>
    </div>
  );
};

export default ChangePasswordPage;
