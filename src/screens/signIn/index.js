import React, { useState } from "react";
import { localization } from "../../modules/localization";
import { useDispatch } from "react-redux";
import teamLogo from "../../assets/svg/teamLogo.svg";
import { signIn } from "../../modules/saga/authorization/action";
import "./styles.css";
import { TopLeftShape } from "../../assets/svg/signIn/topLeftShape";
import  OpenEye from "../../assets/svg/authorization/open_eye";
import  ClosedEye from "../../assets/svg/authorization/closed_eye";

const LogInPage = () => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [isVisible, setIsVisible] = useState(false);

  const dispatch = useDispatch();

  const onPressEnter = e => {
    e.key === "Enter" &&
      dispatch(
        signIn({
          login,
          password
        })
      );
  };

  return (
    <div className="bodiesContainer">
      <div className="teamLogo">
        <img src={teamLogo} />
      </div>
      <div className="inputWindow">
        <input
          onKeyDown={onPressEnter}
          type="text"
          className="loginText"
          placeholder={localization.LOGIN}
          onChange={(event) => setLogin(event.target.value)}
        />
        <div className="passwordHide">
          <input
            onKeyDown={onPressEnter}
            type={isVisible ? "text" : "password"}
            className="passwordText"
            placeholder={localization.PASSWORD}
            onChange={(event) => setPassword(event.target.value)}
          />
          <span
            style={{
              position: "absolute",
              right: 20,
              top: "8%",
              cursor: "pointer",
            }}
            onClick={() => setIsVisible(!isVisible)}
          >
            {isVisible ? <ClosedEye isOpacity={password} color={'#455A64'}/> : <OpenEye isOpacity={password} color={'#455A64'}/>}
          </span>
        </div>
        <div className="signinButton">
          <button onClick={() => dispatch(signIn({ login, password }))}>
            {localization.SIGN_IN_BTN}
          </button>
        </div>
      </div>
      <div
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          zIndex: -1,
        }}
      >
        <TopLeftShape />
      </div>
    </div>
  );
};

export default LogInPage;
