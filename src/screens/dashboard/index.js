import React, { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getTimesheet } from "../../modules/saga/dashboard/action";
import DashboardDate from "./dashboardComponents/date";
import DashboardDifference from "./dashboardComponents/difference";
import DashboardTime from "./dashboardComponents/time";
import DashboardTotal from "./dashboardComponents/total";
import leftImg from "../../assets/svg/dashboard/Group 92.png"
import rightImg from "../../assets/svg/dashboard/Group 93.png"
import ListOfUsers from "../../components/userSelect/index";
import { getUsersList } from '../../modules/saga/userSelect/actions';
import Header from "../../components/header";
import './index.css'
import MonthsCalendar from "../../components/monthsCalendar";

const Dashboard = () => {
    const dispatch = useDispatch();
    useEffect((() => {
        dispatch(getTimesheet())
    }), [dispatch]);

    const [isAdminOrManager, setAdminOrManager] = useState(false);
    const initialData = useSelector(state => state.authReducer.initialData);
    const roleId = initialData[0]?.userProfile?.role_id;

    useEffect(() => {
        if (roleId == "1" || roleId == "2" ) {
            setAdminOrManager(true);
        }
    }, [roleId]);

    useEffect(() => {
        if (isAdminOrManager) {
            dispatch(getUsersList());
        }
    }, [isAdminOrManager]);

    const userSelect = useMemo(() => {
        return <ListOfUsers/>
    }, [isAdminOrManager])

    return (
        <div className='dashboardBody'>
            <Header/>
            <div className='newBlock'>
                <div className='dashboardContainer'>
                <div className='panel'>
                    <MonthsCalendar/>
                    <div className='userSelect' style={{display: (roleId == '1' || roleId == '2') ? 'block' : 'none'}}>         
                        {userSelect}
                    </div>
                </div>
                <div className='dashboardComponents'>
                    <DashboardDate />
                    <DashboardTime />
                    <DashboardTotal />
                    <DashboardDifference />
                </div>
                </div>
                <div>
                    <img className="imageLeft" src={leftImg} />
                    <img className="imageRight" src={rightImg} />
                </div>
            </div>
        </div>
    )
};

export default Dashboard;