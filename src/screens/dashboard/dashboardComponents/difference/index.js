import { useMemo } from "react";
import { useSelector } from "react-redux";
import { totalTimeSelector } from "../../../../modules/redux/reducersAndActions/dashboard/selectors";
import './index.css';

const DashboardDifference = () => {
    const time = useSelector(totalTimeSelector);
    const difference = useMemo(() => {
        return (
            time?.map((item, index) => {
                let result = '';
                if (Number.isNaN(item.time) || item.time == 'unknown') {
                    result = 'unknown'
                } else {
                    const dateCurrent = new Date(`${item.date}T${item.time}`);
                    const dateWorking = new Date(`${item.date}T09:00:00`);
                    const math = Math.abs(dateCurrent - dateWorking);
                    const dateHours = new Date(math).getUTCHours().toString();
                    const dateMinutes = new Date(math).getUTCMinutes().toString();
                    const dateSeconds = new Date(math).getUTCSeconds().toString();
                    const hours = dateHours.length === 1 ? `0${dateHours}` : `${dateHours}`;
                    const minutes = dateMinutes.length === 1 ? `0${dateMinutes}` : `${dateMinutes}`;
                    const seconds = dateSeconds.length === 1 ? `0${dateSeconds}` : `${dateSeconds}`;
                    result = `${dateWorking > dateCurrent ? '-' : '+'}${hours}:${minutes}:${seconds}`;
                }
                return (
                    <div key={index}>
                        <div className="differenceBox">
                            <p>{result}</p>
                        </div>
                    </div>
                )
            })
        )
    }, [time]);

    return (
        <div className="differenceBody">
            <div className="differenceHead">Difference</div>
            <div>{difference}</div>
        </div>
    )
};

export default DashboardDifference;