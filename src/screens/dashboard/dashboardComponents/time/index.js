import { findByLabelText } from "@testing-library/dom";
import { useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { timesheetSelector } from "../../../../modules/redux/reducersAndActions/dashboard/selectors";
import './index.css';

const DashboardTime = () => {
    const dashboardTime = useSelector(timesheetSelector);
    const maxArray = Math.ceil(Math.max.apply(null, dashboardTime?.map(i => i.time.length)) / 2);//find the longest array to display in/out relying on lenght
    const emptyTime = "--:--"
    const rowStyle = {
        display: "flex",
        width: 206 * (maxArray * 2),
    };
    const dashboardData = useMemo(() => {
        return dashboardTime?.map(item => {
            if (item?.time?.length < maxArray * 2) {
                const difference = (maxArray * 2) - item?.time?.length;
                let temp = [];
                for(let i = 0; i < difference; i++) {
                    temp.push({time: emptyTime, isCo: true});
                }
                return {date: item?.date, time: [...item?.time, ...temp]};
            } else {
                return item;
            }
        });
    }, [dashboardTime, maxArray]);

    const getInOutRow = useMemo(() => {
        let result = [];
        for (let i = 0; i < (maxArray >= 2 ? maxArray : 2); i++) {
            const inOutContainer = <div className="timeHeadMain">
                <div className="timeHead" >In</div>
                <div className="timeHead" >Out</div>
            </div>;
            result.push(inOutContainer);
        }
        return result;
    }, [maxArray, dashboardTime]);

    return (
        <div className="timeBody">
            <div className="timeInOutRow">
                {getInOutRow}
            </div>
            {dashboardData.map((item) => {
                if (item.time.length % 2 !== 0) {
                    item.time.push({time: emptyTime})
                }
                return (
                    <div className="timeRow" style={rowStyle}>
                        {item.time.map((elem, index) => {
                            return (
                                <div key={index} className={!(index % 2) ? "timeBoxIn" : "timeBoxOut"}>
                                    <p>{elem.time}</p>
                                </div>
                            )
                        })}
                    </div>
                )
            })}
        </div>
    )
};

export default DashboardTime;