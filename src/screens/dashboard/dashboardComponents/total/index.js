import { useEffect, useMemo, useState } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import { setTotal } from "../../../../modules/redux/reducersAndActions/dashboard/action";
import { timesheetSelector } from "../../../../modules/redux/reducersAndActions/dashboard/selectors";
import './index.css';

const DashboardTotal = () => {
    const dashboardTime = useSelector(timesheetSelector, shallowEqual);
    const [data, setData] = useState([])
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setTotal(data))
    }, [data])
    const renderTotal = useMemo(() => {
        const temp = []
        setData(temp)
        return dashboardTime?.map((item, index) => {
            const date = item.date;
            const time = item.time;
            let result = '';
            let milliSeconds = 0;
            for (let index = 0; index < time.length; index++) {
                if (!(time.length % 2)) {
                    if (time[index].time == '--:--') {
                        result = "unknown"
                    }
                    else if (index && index % 2) {
                        const start = time[index - 1].time;
                        const end = time[index].time;
                        const dateStart = new Date(`${date}T${start}`);
                        const dateEnd = new Date(`${date}T${end}`);
                        const difference = Math.abs(dateEnd - dateStart);
                        milliSeconds = milliSeconds + difference;
                        const formateHours = new Date(milliSeconds).getUTCHours().toString();
                        const formateMinutes = new Date(milliSeconds).getUTCMinutes().toString();
                        const formateSeconds = new Date(milliSeconds).getUTCSeconds().toString();
                        const hours = formateHours.length === 1 ? `0${formateHours}` : formateHours;
                        const minutes = formateMinutes.length === 1 ? `0${formateMinutes}` : formateMinutes;
                        const seconds = formateSeconds.length === 1 ? `0${formateSeconds}` : formateSeconds;
                        result = `${hours}:${minutes}:${seconds}`;
                    }
                }
            }
            temp.push({ date, time: result });
            return (
                <div key={index}>
                    <div className="totalBox">
                        <p>{result}</p>
                    </div>
                </div>
            );
        })
    }, [dashboardTime]);
    return (
        <div className="totalBody">
            <div className="totalHead">Total</div>
            {renderTotal}
        </div>
    )
}

export default DashboardTotal;