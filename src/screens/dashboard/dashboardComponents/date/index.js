import { useSelector } from "react-redux"
import { timesheetSelector } from "../../../../modules/redux/reducersAndActions/dashboard/selectors";
import './index.css';

const DashboardDate = () => {
    const dashboardDate = useSelector(timesheetSelector);
    return (
        <div className="dateBody">
            <div className="dateHead">Date</div>
            {dashboardDate.map((item, index) => {
                return (
                    <div key={index}>
                        <div className="dateBox">
                            <p>{item.date}</p>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default DashboardDate;