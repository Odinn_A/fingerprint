import React, { useMemo } from "react";
import LogInPage from "./screens/signIn";
import "./style.css";
import { BrowserRouter as Route, Switch } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectIsChangePassword, selectIsUserAuthorised } from "./modules/redux/reducersAndActions/authorization/selectors";
import ChangePasswordPage from "./screens/changePass";
import Dashboard from "./screens/dashboard";

const App = () => {
  const isUserAuthorised = useSelector(selectIsUserAuthorised)
  const isChangePassword = useSelector(selectIsChangePassword);
  const authComponent = useMemo(() => {
    return isUserAuthorised && !isChangePassword
      ? <ChangePasswordPage />
      : <LogInPage />
  }, [isChangePassword, isUserAuthorised]);
  return (
    <div>
      <Switch>
        {(isUserAuthorised && isChangePassword) ?
          <Dashboard /> : authComponent}
      </Switch>
    </div>
  );
};

export default App;