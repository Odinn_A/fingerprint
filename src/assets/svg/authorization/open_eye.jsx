import React from "react";

function OpenEye({color, isOpacity}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="40"
      height="40"
      fill="none"
      viewBox="0 0 40 40"
    >
      <path
        fill={color}
        d="M25 20c0 2.757-2.243 5-5 5s-5-2.243-5-5 2.243-5 5-5 5 2.243 5 5zm15-.748s-7.087 12.415-19.975 12.415C8.058 31.667 0 19.252 0 19.252S7.41 8.333 20.025 8.333C32.848 8.333 40 19.252 40 19.252zM28.333 20a8.333 8.333 0 10-16.666 0A8.333 8.333 0 0020 28.333 8.332 8.332 0 0028.333 20z"
        opacity={isOpacity?1:0.5}
      ></path>
    </svg>
  );
}

export default OpenEye;
