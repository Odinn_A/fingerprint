export const BottomRightShape = () => {
  return (
    <svg
      style={{
        position: 'absolute',
        right: 0,
        bottom: 0,
        zIndex: -1
      }}
      width="90%"
      height="90%"
      viewBox="-520 0 829 842"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M252.001 564.341C21.3901 539.904 -10.4571 739.265 2.44571 842H829C829.911 573.764 829 36.5 829 3.99999C829 -19 658.212 72.8538 647.282 298.081C636.353 523.308 540.265 594.888 252.001 564.341Z"
        fill="url(#paint0_linear_588:18)"
      />
      <defs>
        <linearGradient
          id="paint0_linear_588:18"
          x1="414.702"
          y1="0.563965"
          x2="414.702"
          y2="842"
          gradientUnits="userSpaceOnUse"
        >
          <stop stop-color="#91D7E0" />
          <stop offset="1" stop-color="#0360A4" />
        </linearGradient>
      </defs>
    </svg>
  );
};
