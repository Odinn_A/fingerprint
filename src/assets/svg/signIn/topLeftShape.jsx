export const TopLeftShape = () => {
  return (
    <svg
      width="260%"
      height="250%"
      viewBox="0 0 1191 863"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M695.076 659.429C588.068 947.005 117.178 902.743 -0.000671985 690.957L-0.000481402 0.000570742L1190.87 0.000465437C1190.87 160 1175.41 222.131 938.08 231.533C700.746 240.936 758.824 488.108 695.076 659.429Z"
        fill="url(#paint0_linear_588:19)"
      />
      <defs>
        <linearGradient
          id="paint0_linear_588:19"
          x1="1187.13"
          y1="434.949"
          x2="-3.79694"
          y2="424.293"
          gradientUnits="userSpaceOnUse"
        >
          <stop stop-color="#91D7E0" />
          <stop offset="1" stop-color="#0360A4" />
        </linearGradient>
      </defs>
    </svg>
  );
};
