export const apiRoutes = {
  baseUrl: "http://192.168.88.45/xbitbucket/xtimesheet/public",
  signIn: "/login",
  timesheetUrl: "/attendance/viewtime",
  changePassword: "/changeuserpsw",
  gentimesheet: "/gentimesheet",
  usersList: "/getlogin",
};

